package com.example.mykotlinsco.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.mykotlinsco.R
import com.example.mykotlinsco.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_dash_board.*


class DashBoardActivity : BaseActivity() {

    val ABOUT_SCO : Int = 1
    val OUTREACH_CONCERT : Int = 2
    val UPCOMING_CONCERT : Int = 3
    val OUR_MUSIC : Int = 4
    val OTHERS : Int = 5
    var bottomFlag : Int = 3 ;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        updateBottomBarView()
    }

    private fun updateBottomBarView(){
        layoutAboutSco.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        layoutOutreachConcert.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        layoutUpcomingConcert.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        layoutOurMusic.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        layoutOthers.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        ivAboutSco.setImageDrawable(resources.getDrawable(R.drawable.ic_about_sco))
        ivOutreachConcert.setImageDrawable(resources.getDrawable(R.drawable.ic_outreach_concert))
        ivUpcomingConcert.setImageDrawable(resources.getDrawable(R.drawable.ic_upcoming_concert))
        ivOurMusic.setImageDrawable(resources.getDrawable(R.drawable.ic_our_music))
        ivOthers.setImageDrawable(resources.getDrawable(R.drawable.ic_others))

        if(bottomFlag == ABOUT_SCO){
            layoutAboutSco.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_bottombar_selected))
            ivAboutSco.setImageDrawable(resources.getDrawable(R.drawable.ic_about_sco_selected))
        }
        else if(bottomFlag == OUTREACH_CONCERT){
            layoutOutreachConcert.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_bottombar_selected))
            ivOutreachConcert.setImageDrawable(resources.getDrawable(R.drawable.ic_outreach_concert_selected))
        }
        if(bottomFlag == UPCOMING_CONCERT){
            layoutUpcomingConcert.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_bottombar_selected))
            ivUpcomingConcert.setImageDrawable(resources.getDrawable(R.drawable.ic_upcoming_concert_selected))
        }
        if(bottomFlag == OUR_MUSIC){
            layoutOurMusic.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_bottombar_selected))
            ivOurMusic.setImageDrawable(resources.getDrawable(R.drawable.ic_our_music_selected))
        }
        if(bottomFlag == OTHERS){
            layoutOthers.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_bottombar_selected))
            ivOthers.setImageDrawable(resources.getDrawable(R.drawable.ic_others_selected))
        }
    }
}