package com.example.mykotlinsco.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.example.mykotlinsco.R
import com.example.mykotlinsco.utils.BaseActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        val handler : Handler = Handler()
        handler.postDelayed(object : Runnable{
            override fun run() {
                Toast.makeText(this@SplashActivity, "Welcome", Toast.LENGTH_SHORT).show()
                val intent : Intent = Intent(applicationContext, DashBoardActivity::class.java)
                startActivity(intent)
            }
        }, 1000)
    }
}