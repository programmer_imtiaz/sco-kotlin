package com.example.mykotlinsco.utils

import android.content.Context
import android.content.ContextWrapper
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.mykotlinsco.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideStatusbar()
    }

    override fun attachBaseContext(newBase: Context?) {
        val settingSharedPreferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(newBase)
        val language : String? = settingSharedPreferences.getString(newBase?.resources?.getString(R.string.pref_key_language), "en")
        val contextWrapper : ContextWrapper? = MyContextWrapper.wrap(newBase, language)
        val calligraphyContextWrapper : ContextWrapper? = CalligraphyContextWrapper.wrap(contextWrapper)
        super.attachBaseContext(calligraphyContextWrapper)
    }

    fun hideStatusbar(){
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN )
    }
}