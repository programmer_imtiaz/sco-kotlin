package com.example.mykotlinsco.utils

import android.util.Log
import androidx.multidex.MultiDexApplication
import com.orhanobut.logger.Logger
import java.io.*
import java.lang.Exception
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

class App : MultiDexApplication() {

    companion object{

        val show_log : Boolean = true

        //ZIP LINK
        val downloadLink : String = "http://www.sco.com.sg/MusicFile_2.zip"
        val ZipFileName : String = "MusicFile_2.zip"
        val MusicFileName : String = "MusicFile"
        val ASSET_LINK : String = "file:///android_asset/"

        fun showLogE(TAG : String, msg : String){
            if(!show_log) return else Log.e(TAG, msg)
        }

        fun showLogD(TAG : String, msg : String){
            if(!show_log) return else Log.d(TAG, msg)
        }

        fun showLoggerE(TAG : String, msg : String){
            if(!show_log) return else Logger.e(msg)
        }

        fun showLoggerJson(TAG : String, msg : String){
            if(!show_log) return else Logger.json(msg)
        }
        
        @Throws(IOException::class)
        fun unzip(zipFile : File, targetDirectory : File) : Boolean{
            val zipInputStreamObj : ZipInputStream = ZipInputStream(
                BufferedInputStream(FileInputStream(zipFile)))
            try {
                var zipFileLength : Int = zipFile.length().toInt()
                var count : Int
                var zipEntryObj : ZipEntry? = null
                var buffer : ByteArray = ByteArray(zipFileLength)
                zipEntryObj = zipInputStreamObj.nextEntry
                while (zipEntryObj != null){
                    val file : File = File(targetDirectory, zipEntryObj.name)
                    val dir : File? = if(zipEntryObj.isDirectory) file else file.parentFile
                    if(dir != null){
                        if(!dir.isDirectory && !dir.mkdir()){
                            throw FileNotFoundException("Failed to ensure directory: " + dir.absolutePath)
                        }
                    }
                    if (zipEntryObj.isDirectory){
                        continue
                    }

                    val fileOutputStreamObj : FileOutputStream = FileOutputStream(file)
                    count = zipInputStreamObj.read(buffer)
                    try {
                        while (count != -1){
                            fileOutputStreamObj.write(buffer, 0, count)
                            count = zipInputStreamObj.read(buffer) // condition value updating
                        }
                    }
                    finally {
                        fileOutputStreamObj.close()
                    }

                    zipEntryObj = zipInputStreamObj.nextEntry // condition value updating
                }
            }
            catch (exp : Exception){
                return false
            }
            finally {
                zipInputStreamObj.close()
            }
            return true
        }
    }
}