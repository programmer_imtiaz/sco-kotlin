package com.example.mykotlinsco.utils

import android.annotation.TargetApi
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import java.util.*

class MyContextWrapper(base: Context?) : ContextWrapper(base) {

    companion object{
        fun wrap(context: Context?, lang: String?) : ContextWrapper{
            var mContext : Context? = context
            if(mContext != null){
                val config : Configuration = mContext.resources.configuration
                if(!lang.equals("")){
                    val locale : Locale = Locale(lang)
                    Locale.setDefault(locale)
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                        setSystemLocal(config, locale)
                    }
                    else{
                        setSystemLocalLegacy(config, locale)
                    }

                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        config.setLayoutDirection(locale)
                        mContext = mContext.createConfigurationContext(config)
                    }
                    else{
                        mContext.resources.updateConfiguration(config, mContext.resources.displayMetrics)
                    }
                }
            }
            return MyContextWrapper(mContext)
        }

        @TargetApi(Build.VERSION_CODES.N)
        fun setSystemLocal(config : Configuration, local : Locale){
            config.setLocale(local)
        }

        @TargetApi(Build.VERSION_CODES.N)
        fun getSystemLocal(config : Configuration) : Locale{
            return config.locales.get(0)
        }

        @SuppressWarnings("deprecation")
        fun setSystemLocalLegacy(config : Configuration, local : Locale){
            config.locale = local
        }

        @SuppressWarnings("deprecation")
        fun getSystemLocalLegacy(config : Configuration) : Locale{
            return config.locale
        }
    }
}